package com.login.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class DBConnection {
	public static Connection getConnection() throws SQLException {
		Connection connection=null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		String name="root";
		String password="root";
		String url="jdbc:mysql://localhost:3306/user";
		connection=DriverManager.getConnection(url, name, password);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return connection;
	}

}
